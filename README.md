## My resume / CV

2nd project of the OpenClassrrooms' path: Web Developer.

>   Integrate web content according to a mockup using HTML & CSS

🔧 Skills acquired in this project:

 -   Follow code standards in HTML & CSS
 -   Prepare web page elements according to a mockup
 -   Define the structure of a web page according to a mockup
 -   Track a code project with version control on GitHub
 -   Implement a responsive layout


 The result >>> https://oc-web-dev-projects.gitlab.io/cv 